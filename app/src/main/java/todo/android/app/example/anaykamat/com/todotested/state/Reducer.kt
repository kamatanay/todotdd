package todo.android.app.example.anaykamat.com.todotested.state

import todo.android.app.example.anaykamat.com.todotested.kotlin_data.events.Event
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.State

/**
 * Created by anay on 08/08/18.
 */
interface Reducer {
    fun reduce(currentState: State, event:Event): State
}