package todo.android.app.example.anaykamat.com.todotested.screens.splash

import io.reactivex.Observable
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.events.Event
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.Action
import todo.android.app.example.anaykamat.com.todotested.state.Reducer
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.State
import java.util.concurrent.TimeUnit

/**
 * Created by anay on 08/08/18.
 */
class SplashReducer:Reducer {
    override fun reduce(currentState: State, event: Event): State {
        return when(event){
            is Event.LoadData -> currentState.copy(actions = Observable.fromIterable(listOf(Action.ShowCurrentScreen, Action.LoadData(
                    Observable.timer(5, TimeUnit.SECONDS).map { Unit }
            ))))
            Event.DataLoaded -> currentState.copy(currentScreen = currentState.homeScreen, actions = Observable.fromIterable(listOf(Action.ShowCurrentScreen)))
            else -> currentState
        }
    }
}