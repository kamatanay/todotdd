package todo.android.app.example.anaykamat.com.todotested.screens

import android.content.Context
import android.view.View
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.events.Event
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.events.HomeViewEvents
import todo.android.app.example.anaykamat.com.todotested.screens.home.HomeReducer
import todo.android.app.example.anaykamat.com.todotested.screens.home.HomeViewUpdater
import todo.android.app.example.anaykamat.com.todotested.state.Reducer
import todo.android.app.example.anaykamat.com.todotested.state.ViewUpdater
import todo.android.app.example.anaykamat.com.todotested.ui.views.HomeView

/**
 * Created by anay on 09/08/18.
 */
class HomeScreen:Screen {

    private val events: PublishSubject<Event> = PublishSubject.create()

    override fun buildView(context: Context): View {
        return HomeView(context).also {
            it.eventsObservable().subscribe { event ->
                when(event){
                    is HomeViewEvents.DialogDismissed -> events.onNext(Event.DialogCancelled)
                    is HomeViewEvents.NoteSubmitted -> events.onNext(Event.NoteSubmitted(event.note))
                    is HomeViewEvents.AddButtonClicked -> events.onNext(Event.AddButtonClicked)
                }
            }
        }
    }

    override fun eventsObservable(): Observable<Event> = events.hide().share()

    override fun reducer(): Reducer = HomeReducer()

    override fun updater(): ViewUpdater {
        return HomeViewUpdater()
    }
}