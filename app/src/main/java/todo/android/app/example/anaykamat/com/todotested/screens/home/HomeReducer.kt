package todo.android.app.example.anaykamat.com.todotested.screens.home

import io.reactivex.Observable
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.events.Event
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.models.Note
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.Action
import todo.android.app.example.anaykamat.com.todotested.state.Reducer
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.State

/**
 * Created by anay on 09/08/18.
 */
class HomeReducer:Reducer {
    override fun reduce(currentState: State, event: Event): State {
        return when(event){
            is Event.AddButtonClicked -> currentState.copy(actions = Observable.fromIterable(listOf(Action.ShowDialog)))
            is Event.NoteSubmitted -> {
                val (notes, actions) = if (currentState.notes.contains(event.note)){
                    Pair<List<Note>, Observable<Action>>(currentState.notes, Observable.fromIterable(listOf(Action.ShowToast)))
                } else {
                    Pair<List<Note>, Observable<Action>>(currentState.notes + listOf<Note>(event.note), Observable.fromIterable(listOf(Action.CloseDialog, Action.AddNote(event.note))))
                }
                currentState.copy(notes = notes, actions = actions)
            }
            is Event.DialogCancelled -> currentState.copy(actions = Observable.fromIterable(listOf(Action.CloseDialog)))
            else -> currentState
        }
    }
}