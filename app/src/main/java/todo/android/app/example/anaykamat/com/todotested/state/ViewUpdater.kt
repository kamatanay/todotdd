package todo.android.app.example.anaykamat.com.todotested.state

import io.reactivex.Observable
import todo.android.app.example.anaykamat.com.todotested.MainActivity
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.events.Event
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.State

/**
 * Created by anay on 08/08/18.
 */
interface ViewUpdater {
    fun eventsObservable(): Observable<Event>
    fun update(state: State, activity: MainActivity)
}