package todo.android.app.example.anaykamat.com.todotested.screens

import android.content.Context
import android.view.View
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.events.Event
import todo.android.app.example.anaykamat.com.todotested.screens.splash.SplashReducer
import todo.android.app.example.anaykamat.com.todotested.screens.splash.SplashViewUpdater
import todo.android.app.example.anaykamat.com.todotested.state.Reducer
import todo.android.app.example.anaykamat.com.todotested.state.ViewUpdater
import todo.android.app.example.anaykamat.com.todotested.ui.views.SplashView

/**
 * Created by anay on 08/08/18.
 */
class SplashScreen:Screen {

    private val events: PublishSubject<Event> = PublishSubject.create()

    override fun buildView(context:Context): View {
        return SplashView(context)
    }

    override fun eventsObservable(): Observable<Event> {
        return events.hide().share()
    }

    override fun reducer():Reducer {
        return SplashReducer()
    }

    override fun updater(): ViewUpdater {
        val splashViewUpdater = SplashViewUpdater()
        splashViewUpdater.eventsObservable().subscribe { event ->
            events.onNext(event)
        }
        return splashViewUpdater
    }

}