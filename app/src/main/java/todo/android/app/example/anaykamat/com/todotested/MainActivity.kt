package todo.android.app.example.anaykamat.com.todotested

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.LinearLayout
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.events.Event
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.State


open class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val activity = this

        val (initialState, listOfEventObservables) = buildInitialState()

        Observable.merge(listOfEventObservables)
                .scan(initialState, { state, event ->
                     state.currentScreen.reducer().reduce(state, event)
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { state ->
                    state.currentScreen.updater().update(state, activity)
                }
    }

    open fun currentView(): View {
        return findViewById<LinearLayout>(R.id.rootLayout).getChildAt(0)
    }

    fun updateView(view:View){
        findViewById<LinearLayout>(R.id.rootLayout).removeAllViews()
        findViewById<LinearLayout>(R.id.rootLayout).addView(view)
    }

    open protected fun buildInitialState():Pair<State, List<Observable<out Event>>>{
        return State().let { it.copy(currentScreen = it.splashScreen) }.let {
            Pair(it, listOf(it.splashScreen.eventsObservable(), it.homeScreen.eventsObservable(), Observable.just(Event.LoadData)))
        }
    }
}
