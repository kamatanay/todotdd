package todo.android.app.example.anaykamat.com.todotested.kotlin_data.events

sealed class DialogEvents {
    data class OkClicked(val note:String): DialogEvents()
    object CancelClicked: DialogEvents()
}