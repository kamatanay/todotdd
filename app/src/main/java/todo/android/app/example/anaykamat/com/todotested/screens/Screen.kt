package todo.android.app.example.anaykamat.com.todotested.screens

import android.content.Context
import android.view.View
import io.reactivex.Observable
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.events.Event
import todo.android.app.example.anaykamat.com.todotested.state.Reducer
import todo.android.app.example.anaykamat.com.todotested.state.ViewUpdater

/**
 * Created by anay on 08/08/18.
 */
interface Screen {
    fun buildView(context: Context): View
    fun eventsObservable(): Observable<Event>
    fun reducer(): Reducer
    fun updater(): ViewUpdater
}