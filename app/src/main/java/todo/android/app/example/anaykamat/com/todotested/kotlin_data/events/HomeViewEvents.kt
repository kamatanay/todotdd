package todo.android.app.example.anaykamat.com.todotested.kotlin_data.events

import todo.android.app.example.anaykamat.com.todotested.kotlin_data.models.Note

sealed class HomeViewEvents {
    object AddButtonClicked: HomeViewEvents()
    data class NoteSubmitted(val note: Note): HomeViewEvents()
    object DialogDismissed: HomeViewEvents()
}