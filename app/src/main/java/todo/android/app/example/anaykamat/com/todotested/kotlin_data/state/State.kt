package todo.android.app.example.anaykamat.com.todotested.kotlin_data.state

import io.reactivex.Observable
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.models.Note
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.Action
import todo.android.app.example.anaykamat.com.todotested.screens.HomeScreen
import todo.android.app.example.anaykamat.com.todotested.screens.Screen
import todo.android.app.example.anaykamat.com.todotested.screens.SplashScreen

/**
 * Created by anay on 08/08/18.
 */
data class State(val currentScreen:Screen = SplashScreen(),
                 val splashScreen: SplashScreen = SplashScreen(),
                 val homeScreen: HomeScreen = HomeScreen(),
                 val dataLoaded:Boolean = false,
                 val actions: Observable<Action> = Observable.fromIterable(emptyList<Action>()),
                 val notes:List<Note> = emptyList())