package todo.android.app.example.anaykamat.com.todotested.screens.home

import junit.framework.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import todo.android.app.example.anaykamat.com.todotested.BuildConfig
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.events.Event
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.models.Note
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.Action
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.State

/**
 * Created by anay on 09/08/18.
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class)
class HomeReducerTest {

    fun reducer():HomeReducer = HomeReducer()

    @Test
    fun itShouldLoadShowDialogOnClickOfAddButton(){
        val currentState = State()

        val newState = reducer().reduce(currentState, Event.AddButtonClicked)

        val actionList = newState.actions.toList()
        Assert.assertEquals(1, actionList.blockingGet().count())
        Assert.assertTrue(actionList.blockingGet().find { action -> action.equals(Action.ShowDialog) } != null)
    }

    @Test
    fun itShouldCloseTheDialogAndAddNoteOnceNoteIsSubmittedFromDialog(){
        val currentState = State()
        val note = Note("First")

        val newState = reducer().reduce(currentState, Event.NoteSubmitted(note))

        val actionList = newState.actions.toList()
        Assert.assertEquals(2, actionList.blockingGet().count())
        Assert.assertTrue(actionList.blockingGet().find { action -> action.equals(Action.AddNote(note)) } != null)
        Assert.assertTrue(actionList.blockingGet().find { action -> action.equals(Action.CloseDialog) } != null)
    }

    @Test
    fun itShouldCloseTheDialogOnceDialogIsCancelled(){
        val currentState = State()

        val newState = reducer().reduce(currentState, Event.DialogCancelled)

        val actionList = newState.actions.toList()
        Assert.assertEquals(1, actionList.blockingGet().count())
        Assert.assertTrue(actionList.blockingGet().find { action -> action.equals(Action.CloseDialog) } != null)
    }

    @Test
    fun itShouldStoreTheNewlyAddedNoteInTheState(){
        val firstNote = Note("First")
        val state = State(notes = listOf<Note>(firstNote))
        val newNote = Note("Second")

        val newState = reducer().reduce(state, Event.NoteSubmitted(newNote))

        Assert.assertEquals(2, newState.notes.count())
        Assert.assertEquals(listOf(firstNote, newNote), newState.notes)
    }

    @Test
    fun itNotAddNoteToStateIfItExists(){
        val note = Note("First")
        val currentState = State(notes = listOf<Note>(note))
        val newNote = Note("First")

        val newState = reducer().reduce(currentState, Event.NoteSubmitted(newNote))

        Assert.assertEquals(1, newState.notes.count())
        Assert.assertEquals(listOf(note), newState.notes)
    }

    @Test
    fun itShouldNotCloseTheDialogAndAddNoteIfNoteSubmittedExists(){
        val note = Note("First")
        val currentState = State(notes = listOf(note))
        val newNote = Note("First")

        val newState = reducer().reduce(currentState, Event.NoteSubmitted(newNote))

        val actionList = newState.actions.toList()
        Assert.assertNull(actionList.blockingGet().find { action -> action.equals(Action.AddNote(newNote)) })
        Assert.assertNull(actionList.blockingGet().find { action -> action.equals(Action.CloseDialog) })
    }

    @Test
    fun itShouldGiveAToastMessageSayingAlreadyAdded(){
        val note = Note("First")
        val currentState = State(notes = listOf(note))
        val newNote = Note("First")

        val newState = reducer().reduce(currentState, Event.NoteSubmitted(newNote))

        val actionList = newState.actions.toList()
        Assert.assertNotNull(actionList.blockingGet().find { action -> action.equals(Action.ShowToast) })
    }
}