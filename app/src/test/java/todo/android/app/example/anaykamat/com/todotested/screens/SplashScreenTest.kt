package todo.android.app.example.anaykamat.com.todotested.screens

import junit.framework.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import todo.android.app.example.anaykamat.com.todotested.BuildConfig
import todo.android.app.example.anaykamat.com.todotested.MainActivity
import todo.android.app.example.anaykamat.com.todotested.ui.views.SplashView

/**
 * Created by anay on 08/08/18.
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class)
class SplashScreenTest {

    private fun screen():SplashScreen = SplashScreen()

    @Test
    fun itShouldBuildAndProvideSplashView(){
        val splashView = screen().buildView(Robolectric.buildActivity(MainActivity::class.java).get()) as SplashView
        Assert.assertNotNull(splashView)
    }

}