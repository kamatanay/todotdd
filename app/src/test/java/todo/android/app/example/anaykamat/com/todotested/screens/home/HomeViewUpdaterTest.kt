package todo.android.app.example.anaykamat.com.todotested.screens.home

import io.reactivex.Observable
import junit.framework.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowToast
import todo.android.app.example.anaykamat.com.todotested.BuildConfig
import todo.android.app.example.anaykamat.com.todotested.MainActivity
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.models.Note
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.Action
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.State
import todo.android.app.example.anaykamat.com.todotested.ui.views.HomeView

/**
 * Created by anay on 09/08/18.
 */

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class)
class HomeViewUpdaterTest {


    private fun viewUpdater():HomeViewUpdater = HomeViewUpdater()

    @Test
    fun showDialogShouldShowTheDialogToAddTodo(){
        val mockHomeView = Mockito.mock(HomeView::class.java)

        val mainActivity = Mockito.mock(MainActivity::class.java)

        Mockito.`when`(mainActivity.currentView()).thenReturn(mockHomeView)

        val state = State(actions = Observable.just(Action.ShowDialog))

        viewUpdater().update(state, mainActivity)

        Mockito.verify(mockHomeView, Mockito.times(1)).showDialogForNewTodo(mainActivity)

    }

    @Test
    fun closeDialogShouldDismissTheDialogToAddTodo(){
        val mockHomeView = Mockito.mock(HomeView::class.java)

        val mainActivity = Mockito.mock(MainActivity::class.java)

        Mockito.`when`(mainActivity.currentView()).thenReturn(mockHomeView)

        val state = State(actions = Observable.just(Action.CloseDialog))

        viewUpdater().update(state, mainActivity)

        Mockito.verify(mockHomeView, Mockito.times(1)).dismissDialog()

    }

    @Test
    fun addNoteShouldAddTheNoteInTheView(){

        val note = Note("Hello")
        val mockHomeView = Mockito.mock(HomeView::class.java)

        val mainActivity = Mockito.mock(MainActivity::class.java)

        Mockito.`when`(mainActivity.currentView()).thenReturn(mockHomeView)

        val state = State(actions = Observable.just(Action.AddNote(note)))

        viewUpdater().update(state, mainActivity)

        Mockito.verify(mockHomeView, Mockito.times(1)).add(note)
    }

    @Test
    fun showToastShouldShowAToastSayingAlreadyAdded(){
        val mainActivity = Robolectric.buildActivity(MainActivity::class.java)
                .create()
                .start()
                .resume()
                .visible()
                .get()

        val state = State(actions = Observable.just(Action.ShowToast))

        viewUpdater().update(state, mainActivity)

        val toast = ShadowToast.getLatestToast()
        val toastText = ShadowToast.getTextOfLatestToast()

        Assert.assertNotNull(toast)
        Assert.assertEquals("Already Added",toastText)
    }
}