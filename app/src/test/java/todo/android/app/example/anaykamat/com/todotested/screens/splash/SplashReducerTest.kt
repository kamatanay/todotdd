package todo.android.app.example.anaykamat.com.todotested.screens.splash

import junit.framework.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import todo.android.app.example.anaykamat.com.todotested.BuildConfig
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.events.Event
import todo.android.app.example.anaykamat.com.todotested.screens.HomeScreen
import todo.android.app.example.anaykamat.com.todotested.screens.Screen
import todo.android.app.example.anaykamat.com.todotested.screens.SplashScreen
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.Action
import todo.android.app.example.anaykamat.com.todotested.kotlin_data.state.State
import java.util.concurrent.TimeUnit

/**
 * Created by anay on 08/08/18.
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class)
class SplashReducerTest {

    private fun reducer():SplashReducer = SplashReducer()

    private fun initialState(): State {
        val screens = listOf<Screen>(SplashScreen(), HomeScreen())
        return State(currentScreen = screens.first(), splashScreen = screens.first() as SplashScreen, homeScreen = screens.last() as HomeScreen)
    }

    @Test
    fun itShouldShowSplashScreenAndLoadData(){
        val reducer = reducer()
        val nextState = reducer.reduce(initialState(), Event.LoadData)

        Assert.assertEquals(nextState.splashScreen, nextState.currentScreen)
        val actionList = nextState.actions.toList()
        Assert.assertEquals(2, actionList.blockingGet().count())
        Assert.assertTrue(actionList.blockingGet().find { action -> action.equals(Action.ShowCurrentScreen) } != null)
        val loadDataAction = actionList.blockingGet().find { action -> action is Action.LoadData }
        Assert.assertTrue(loadDataAction != null)
        (loadDataAction as Action.LoadData).dataLoader.timeout(6, TimeUnit.SECONDS).blockingFirst()
    }

    @Test
    fun itShouldSwitchToHomeScreenOnDataLoad(){
        val reducer = reducer()
        val nextState = reducer.reduce(initialState(), Event.DataLoaded)

        Assert.assertEquals(nextState.homeScreen, nextState.currentScreen)
        val actionList = nextState.actions.toList()
        Assert.assertEquals(1, actionList.blockingGet().count())
        Assert.assertTrue(actionList.blockingGet().find { action -> action.equals(Action.ShowCurrentScreen) } != null)
    }

}